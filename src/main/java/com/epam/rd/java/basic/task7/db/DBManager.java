package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (Objects.isNull(instance)) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {}


	/* ---------- Task 1 ---------- */

	public boolean insertUser(User user) throws DBException {

		if (Objects.isNull(user)) throw new IllegalArgumentException();

		try(Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			PreparedStatement stmt = con.prepareStatement(Constants.INSERT_USER1, Statement.RETURN_GENERATED_KEYS);
			) {

			stmt.setString(1, user.getLogin());

			try {
				stmt.executeUpdate();
				try (ResultSet rs = stmt.getGeneratedKeys()) {
					if (rs.next()) {
						user.setId(rs.getInt(1));
					}
					return true;
				}
			} catch(SQLException e) {
				user.setId( getUser(user.getLogin()).getId() );
				return false;
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Exception with inserting new User to the db.", e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> result = new ArrayList<>();

		try(Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			PreparedStatement stmt = con.prepareStatement(Constants.FIND_ALL_USERS);
		) {
			try(ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					result.add(new User(rs.getInt("id"), rs.getString("login")));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Exception with finding all the users", e);
		}

		return result;
	}





	/* ---------- Task 2 ---------- */

	public boolean insertTeam(Team team) throws DBException {
		if (Objects.isNull(team)) throw new IllegalArgumentException();

		try(Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			PreparedStatement stmt = con.prepareStatement(Constants.INSERT_TEAM1, Statement.RETURN_GENERATED_KEYS);
		) {
			stmt.setString(1, team.getName());

			try {
				stmt.executeUpdate();
				try (ResultSet rs = stmt.getGeneratedKeys()) {
					if (rs.next()) {
						team.setId(rs.getInt(1));
					}
					return true;
				}
			} catch(SQLException e) {
				team.setId( getTeam(team.getName()).getId() );
				return false;
			}

		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Exception with inserting new Team to the db.", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> result = new ArrayList<>();

		try(Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			PreparedStatement stmt = con.prepareStatement(Constants.FIND_ALL_TEAMS);
		) {
			try(ResultSet rs = stmt.executeQuery()) {
				while(rs.next()) {
					result.add(new Team(rs.getInt("id"), rs.getString("name")));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Exception with finding all the teams", e);
		}

		return result;
	}




	/* ---------- Task 3 ---------- */

	public User getUser(String login) throws DBException {
		if (Objects.isNull(login)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.GET_USER_BY_NAME);
			) {
			stmt.setString(1, login);

			try(ResultSet rs = stmt.executeQuery()) {
				User user = null;
				if (Objects.nonNull(rs) && rs.next()) {
					user = new User(rs.getInt("id"), rs.getString("login"));
				}
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Got some error with getting user from the DB", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		if (Objects.isNull(name)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.GET_TEAM_BY_NAME)
		) {
			stmt.setString(1, name);

			try(ResultSet rs = stmt.executeQuery()) {
				Team team = null;
				if (Objects.nonNull(rs) && rs.next()) {
					team = new Team(rs.getInt("id"), rs.getString("name"));
				}
				return team;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Got some error with getting team from the DB", e);
		}
	}


	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (Objects.isNull(user) || Objects.isNull(teams)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.SET_TEAM_FOR_USER);)
		{
			try {
				con.setAutoCommit(false);
				stmt.setInt(1, user.getId());

				int count = 0;
				for (Team team: teams) {
					if (Objects.isNull(team)) throw new IllegalArgumentException();
					stmt.setInt(2, team.getId());
					count += stmt.executeUpdate();
				}
				con.commit();
				return count > 0;

			} catch (SQLException e) {
				con.rollback();
				throw e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Got problems with setting a team for user", e);
		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		if (Objects.isNull(user)) throw new IllegalArgumentException();

		List<Team> teams = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.GET_USER_TEAMS);
			) {
			stmt.setInt(1, user.getId());

			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					teams.add(new Team(rs.getInt("id"), rs.getString("name")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Got problems with getting teams for a user", e);
		}
		return teams;
	}



	/* ---------- Task 4 ---------- */

	public boolean deleteTeam(Team team) throws DBException {
		if (Objects.isNull(team)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.DELETE_TEAM_BY_NAME);
		) {
			stmt.setString(1, team.getName());
			int count = stmt.executeUpdate();
			return count > 0;
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Problems with deleting a team", e);
		}
	}




	/* ---------- Task 5 ---------- */

	public boolean updateTeam(Team team) throws DBException {
		if (Objects.isNull(team)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.UPDATE_TEAM);
		) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			int count = stmt.executeUpdate();
			return count > 0;
		} catch(SQLException e){
			e.printStackTrace();
			throw new DBException("Problems with updating the user", e);
		}
	}



	/* ---------- Task 6 ---------- */

	public boolean deleteUsers(User... users) throws DBException {
		if (Objects.isNull(users)) throw new IllegalArgumentException();

		try (Connection con = DriverManager.getConnection(Constants.FINAL_URL);
			 PreparedStatement stmt = con.prepareStatement(Constants.DELETE_USER);
		) {
			con.setAutoCommit(false);
			try{
				int count = 0;
				for (User user: users) {
					if (Objects.isNull(user)) throw new IllegalArgumentException();
					stmt.setInt(1, user.getId());
					count += stmt.executeUpdate();
				}
				con.commit();
				return count == users.length;
			} catch (SQLException e) {
				con.rollback();
				throw e;
			}
		} catch(SQLException e){
			e.printStackTrace();
			throw new DBException("Didn't manage to delete all defined users", e);
		}
	}

}
