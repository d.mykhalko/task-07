package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Constants {

    // QUERIES

    public final static String INSERT_USER1 = "INSERT INTO users VALUES (DEFAULT,?)";
//    public final static String INSERT_USER = "INSERT IGNORE INTO users VALUES (DEFAULT,?);";
    public final static String FIND_ALL_USERS = "SELECT * FROM users u ORDER BY u.id";

    public final static String INSERT_TEAM1 = "INSERT INTO teams VALUES (DEFAULT,?)";
//    public final static String INSERT_TEAM = "INSERT IGNORE INTO teams VALUES (DEFAULT,?);";
    public final static String FIND_ALL_TEAMS = "SELECT * FROM teams t ORDER BY t.id";

    public final static String GET_USER_BY_NAME = "SELECT * FROM users u WHERE u.login=?";
    public final static String GET_TEAM_BY_NAME = "SELECT * FROM teams t WHERE t.name=?";

    public final static String SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES(?,?)";
    public final static String GET_USER_TEAMS = "SELECT t.id, t.name FROM teams t " +
            "INNER JOIN users_teams ut ON t.id=ut.team_id " +
            "WHERE ut.user_id=?";


    public final static String DELETE_TEAM_BY_NAME = "DELETE FROM teams t WHERE t.name=?";

    public final static String UPDATE_TEAM = "UPDATE teams t SET t.name=? WHERE t.id=?";

    public final static String DELETE_USER = "DELETE FROM users u WHERE u.id=?";


    // FIELDS
    private static final Properties PROPERTIES = new Properties();


    public static String FINAL_URL;

    static {
        try(FileReader fr = new FileReader("app.properties")) {
            PROPERTIES.load(fr);
            FINAL_URL = PROPERTIES.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(FINAL_URL);
    }

}
